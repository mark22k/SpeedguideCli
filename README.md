# SpeedguideCli

Unofficial implementation of the client, which gets information about a port using the Speedguide database.

Speedguide Port Database: [https://www.speedguide.net/ports.php](https://www.speedguide.net/ports.php)

## Usage

Get information about port 1:
```
$ /path/to/speedguide 1
```

## Installation

Required gems:
- `async-http`
  - Needed to send the request to the Speedguide port database
- `nokogiri`
  - Required to interpret the response of the database

```
$ wget https://codeberg.org/mark22k/SpeedguideCli/raw/branch/main/speedguide
$ chmod +x speedguide
```

